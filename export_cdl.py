import os
import tempfile
import cdl_convert as cdl

__author__ = "Alex Golding"
__version__ = "2023.06.04.01"


pm = resolve.GetProjectManager()
project = pm.GetCurrentProject()
mp = project.GetMediaPool()
tl = project.GetCurrentTimeline()
ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

# Get a temporary edl_with_cdl

# First a temp dir to export to
temp_dir = tempfile.TemporaryDirectory()
tmpEDL = os.path.join(temp_dir.name, tl.GetName() + '.edl')
print(tmpEDL)

# Export the edl_with_cdl
tl.Export(tmpEDL, resolve.EXPORT_EDL, resolve.EXPORT_CDL)

# Import it back as a colour collection
collection = cdl.parse_file(tmpEDL)

# clean after ourselves
temp_dir.cleanup()

# Global variables we need for the UI
idDict = {}     # This will be used later to reference cdls by id (reel name)
export_folder_path = os.path.join(os.path.expanduser('~'), 'Documents', 'Resolve_exports')

main_exports_window = disp.AddWindow({ "WindowTitle": f"Export CDL Version {__version__}", "ID": "ProjExportsWin", "Geometry": [ 300, 200, 900, 640 ], },
    [
        ui.VGroup({ "Spacing": 10, },
            [
                ui.Label({ "ID": "Author_label", "Text": f"Written by {__author__}", "Weight": 0.0,}),
                ui.HGroup({ "Spacing": 0, "Weight": 0.0, },
                [
                    ui.Label({ "ID": "LocLabel", "Text": "Export to: ", "Weight": 0.0, }),
                    ui.Label({ "ID": "LocExpLabel", "Text": export_folder_path, "Weight": 1.0, }),
                    ui.Button({ "ID": "SaveTo", "Text": "Change", "Weight": 0.0,})
                ]),
                                ui.HGroup({ "Spacing": 0,"Weight": 1.0, },
                    [
                        ui.Tree({ "ID": "ProjectsTree", "Weight": 1.0,}),
                        #ui.ComboBox({ "ID": "MyCombo",}),
                    ]),
                ui.HGroup({ "Spacing": 30, "Weight": 0.0, },
                [
                    ui.Button({ "ID": "CancelBtn", "Text": "Cancel", "Weight": 0.0}),
                    ui.Button({ "ID": "ExportBtn", "Text": "Export All", "Weight": 0.0})
                ]),
            ]),
    ])

# Setup callbacks for our main window

# This is really important, without it we can't close the window!!!
def _func(ev):
    disp.ExitLoop()
main_exports_window.On.ProjExportsWin.Close = _func

# Next we set up the button callback to get a folder
def _func(ev):
    global export_folder_path   # We need to get the global variable as we can't return values from a callback 
    print(export_folder_path)
    export_folder_path = app.RequestDir(itm['LocExpLabel'].Text)
    itm['LocExpLabel'].Text = export_folder_path
main_exports_window.On.SaveTo.Clicked = _func

def _func(ev):
    print("Cancelled")
    disp.ExitLoop()
main_exports_window.On.CancelBtn.Clicked = _func

def _func(ev):
    global export_folder_path
    global idDict
    sel = tree.CurrentItem().Text[0]
    idDict[sel].determine_dest('cdl', export_folder_path)
    print(idDict[sel].file_out)
    cdl.write_cdl(idDict[sel])
main_exports_window.On.ProjectsTree.ItemDoubleClicked = _func


def _func(ev):
	# TODO add subfolder to export all
    global export_folder_path
    global collection
    for c in collection.color_corrections:
        c.determine_dest('cdl', export_folder_path)
        cdl.write_cdl(c)
    disp.ExitLoop()
main_exports_window.On.ExportBtn.Clicked = _func

itm = main_exports_window.GetItems()

itm['ProjectsTree']({'Events': { 'ItemDoubleClicked': True}})

tree = itm['ProjectsTree']

hdr = tree.NewItem()
hdr.Text[0] = 'Reel Name'
hdr.Text[1] = 'Slope'
hdr.Text[2] = 'Offset'
hdr.Text[3] = 'Power'
hdr.Text[4] = 'SAT'

tree.SetHeaderItem(hdr)
tree.ColumnWidth[0] = 180
tree.ColumnWidth[1] = 200
tree.ColumnWidth[2] = 200
tree.ColumnWidth[3] = 200
tree.ColumnWidth[4] = 50

def val_to_string(val):    
    ret_str = f"({val[0]},{val[1]},{val[2]})"
    return ret_str

treeItm = {}

for c in collection.color_corrections:
    reel = c.id
    idDict[reel] = c
    treeItm[reel] = tree.NewItem()
    treeItm[reel].Text[0] = reel
    treeItm[reel].Text[1] = val_to_string(c.slope)
    treeItm[reel].Text[2] = val_to_string(c.offset)
    treeItm[reel].Text[3] = val_to_string(c.power)
    treeItm[reel].Text[4] = str(c.sat)
    tree.AddTopLevelItem(treeItm[reel])

main_exports_window.Show()
disp.RunLoop()
main_exports_window.Hide()
