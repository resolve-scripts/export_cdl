# export_cdl

Copyright (c) 2023 Alex Golding

## Description
This script is designed to be run from within DaVinci Resolve. It will display and allow for easy export of any CDL values from the current timeline.

WARNING Resolve is not designed with exporting CDL in mind, there are plenty of controls that are not reflected in CDL values. It is up to the user to ensure they know and understand the possible pitfalls involved and to have tested any workflow.


## Installation
If it is not your machine please check with your sys admin before continuing, I cannot be held responsible for any problems or damage to your install (see License).

To install simply copy the script to "Fusion/Scripts/Comp" folder relevant to your Resolve install. In order for this to work you must also have Python >= 3.6 installed on your machine and the cdl_convert library (Resolve 17 requires 3.6, Resolve 18+ may use any version >3.6). cdl_convert can be installed by running `pip install cdl_convert`.

To check scripting and cdl_convert are working correctly in Resolve open the console from the workspace dropdown menu, select py3 and issue the command import cdl_convert, if no errors occur you should be good to go.

## Usage
Run the script from Workspace->Scripts->Comp menu. 


## Support
Please ensure that you have Python and that cdl_convert correctly installed and working with DaVinci Resolve as detailed in the Installation section before raising an issue. If you are still having trouble please raise an issue [here](https://gitlab.com/resolve-scripts/export_cdl/-/issues) including a copy of the console output from Resolve.

## Roadmap
This script is considered as pretty much finished, I intend to maintain it for compatibility with any future Resolve updates if possible. Beyond that support for exporting different types of CDL file may be added, but that's about it.


## Authors and acknowledgment
This script relies on cdl_convert for the underlying CDL implementation, with thanks to Sean Wallitsch for releasing it under an MIT License

Please see the [cdl_convert repository](http://github.com/shidarin/cdl_convert/) for more information and full License text.

## License
MIT License

Copyright (c) 2023 Alex Golding

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

